package cassandra;
//import java.io.Externalizable;
//import java.io.IOException;
//import java.io.ObjectInput;
//import java.io.ObjectOutput;

import java.util.Date;

public class PersonId {// implements Externalizable {

    private String firstName;
    private String lastName;
    private int age;
    private boolean married;
    private long height;
    private float weight;
    private Date birthDate;

    public void setFirstName(String name) {
        firstName = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String name) {
        lastName = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public boolean getMarried() {
        return married;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getHeight() {
        return height;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getWeight() {
        return weight;
    }

    public void setBirthDate(Date date) {
        birthDate = date;
    }

    public Date getBirthDate() {
        return birthDate;
    }
//    @Override
//    public void writeExternal(ObjectOutput out) throws IOException {
//        out.writeObject(firstName);
//        out.writeObject(lastName);
//        out.writeInt(age);
//        out.writeBoolean(married);
//        out.writeLong(height);
//        out.writeFloat(weight);
//        out.writeObject(birthDate);
//    }
//    @Override
//    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//        firstName = (String)in.readObject();
//        lastName = (String)in.readObject();
//        age = in.readInt();
//        married = in.readBoolean();
//        height = in.readLong();
//        weight = in.readFloat();
//        birthDate = (Date)in.readObject();
//    }

    public PersonId() {
    }

    /** */
    public PersonId(String firstName, String lastName, int age, boolean married,

                    long height, float weight, Date birthDate) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.married = married;
        this.height = height;
        this.weight = weight;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " " + age + " " + height + " " + weight + " " + birthDate;
    }
}